import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import "@/assets/sass/style.scss";
import "@/assets/sass/base/_typo.scss";
import "@/assets/sass/components/_buttons.scss";

const app = createApp(App)
app.use(router)
app.mount('#app')
