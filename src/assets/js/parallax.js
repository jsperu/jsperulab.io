window.addEventListener('scroll', function(e) {
	var yPos = -(window.pageYOffset / 5);
	var coords = '50% ' + yPos + 'px';
	var backgrounds = document.querySelectorAll("[data-type='background']");

	[].forEach.call(backgrounds, function(background) {
		background.style.backgroundPosition = coords;
	});
});