import { createWebHistory, createRouter } from "vue-router";
import Home from "../pages/Home.vue";
import Contact from "../pages/Contact.vue";
import Success from "../pages/Success.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/contact",
        name: "Contact",
        component: Contact,
    },
    {
        path: "/contact-ok",
        name: "Success",
        component: Success,
    }

];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;