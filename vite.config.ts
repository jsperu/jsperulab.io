import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default ({mode}) => {

  return defineConfig({
    server: {
      port: 3000,
      host: "localhost",
    },
    preview: {
      port: 3000
    },
    resolve: {
      alias: {
        "@/": `${resolve(__dirname, "src")}/`,
      }
    },
    plugins: [vue()],
    build : {
      outDir : "public",
      cssCodeSplit: false
    },
    publicDir: "docs",
    css: {
      preprocessorOptions: {
        scss: {
          api: "modern-compiler",
          additionalData: `
          @use "@/assets/sass/base/_variables.scss" as *;
          @use "@/assets/sass/base/_mq.scss" as *;
          `,
        }
      }
    }
  })
}
